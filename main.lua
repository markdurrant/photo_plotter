local opts = require('src.parse_options')
local process_image = require('src.process_image')
local image_to_paths = require('src.image_to_paths')
local process_paths = require('src.process_paths')
local render = require('src.render')


-- Load input image as a Vips object
local img = process_image.load_image(opts)
      
-- Do edge detection and get paths
local edge_img = process_image.detect_edges(img, opts)
local edge_paths = image_to_paths(edge_img, opts)

-- Do threshhold and get paths
-- local threshhold_img = process_image.threshold(img, opts)
-- local threshhold_paths = image_to_paths(threshhold_img, opts)
-- local vertical_lines = process_paths.vertical_lines(threshhold_paths, opts)

-- local scratch_paths = process_paths.scratch(threshhold_paths, opts)

local grid = process_image.get_grid(process_image.threshold(img, opts), opts)
local grid_paths = process_paths.grid_short_lines(grid, opts)

render({
  edges = edge_paths,
  vertical_lines = vertical_lines,
  scratch = scratch_paths,
  grid_paths = grid_paths
}, opts)
