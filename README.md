# PlotterBooth

**Photo > Paths > Ploter.** Takes a photo and creates .hpgl plotter instructions. Also includes mini plotter server to plot files automatically. 

## Dependancies

#### Lua stuff
* [Luajit](http://luajit.org)
* [LuaRocks](https://luarocks.org)
* [Lua Vips](https://github.com/libvips/lua-vips)
* [LuaXML](https://github.com/LuaDist/luaxml)
* [Pequod](http://pequod.superduper.cool/)

#### Python stuff 
* **Python 2.6.9**
* [chiplotle](http://sites.music.columbia.edu/cmc/chiplotle/)
* [watchdog](https://pythonhosted.org/watchdog/)

#### Other stuff
* [Lib Vips](https://jcupitt.github.io/libvips/)
* [Autotrace](https://github.com/autotrace/autotrace)
* [gphoto2](http://gphoto.org)


## To run
```
# Start plotter server
> python plotter_server.py 
# Create .hpgl file for photo
> luajit main.lua [options] [input_file]
```

## Show options
```
> luajit main.lua --help
```

