import os
import sys
import time
import logging
from chiplotle import *
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

plotter = instantiate_plotters()[0]

print("\n\n")
print("=====================")
print("PLOTTER SERVER RUNING")
print("=====================")

class SendToPlotter(FileSystemEventHandler):
    def on_modified(self, event):
        if os.path.splitext(event.src_path)[1] == '.hpgl':
            plotter.write_file(event.src_path)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    event_handler = SendToPlotter()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()