function plotter
  while true
    echo -e "\033[0;32m\n\n============================"
    echo -e "\033[0;32m    PRESS ENTER TO PLOT!    "
    echo -e "\033[0;32m============================\n"
    read -l -P ""
    
    if count $argv > /dev/null
      eval $argv
    else 
      eval luajit main.lua
    end
  end
end