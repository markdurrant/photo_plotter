local hp1345a_glyphs = {
  [32] = {
    character = "",
    paths = {
    },
    width = 12
  },
  [33] = {
    character = "!",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 1
          },
          {
            y = 0,
            x = 1
          }
        }
      },
      {
        points = {
          {
            y = 5,
            x = 1
          },
          {
            y = 18,
            x = 1
          }
        }
      }
    },
    width = 2
  },
  [34] = {
    character = "\"",
    paths = {
      {
        points = {
          {
            y = 14,
            x = 1
          },
          {
            y = 18,
            x = 2
          }
        }
      },
      {
        points = {
          {
            y = 14,
            x = 5
          },
          {
            y = 18,
            x = 6
          }
        }
      }
    },
    width = 7
  },
  [35] = {
    character = "#",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 2
          },
          {
            y = 18,
            x = 4
          }
        }
      },
      {
        points = {
          {
            y = 0,
            x = 8
          },
          {
            y = 18,
            x = 10
          }
        }
      },
      {
        points = {
          {
            y = 13,
            x = 0
          },
          {
            y = 13,
            x = 12
          }
        }
      },
      {
        points = {
          {
            y = 5,
            x = 0
          },
          {
            y = 5,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [36] = {
    character = "$",
    paths = {
      {
        points = {
          {
            y = 3,
            x = 0
          },
          {
            y = 1,
            x = 3
          },
          {
            y = 1,
            x = 9
          },
          {
            y = 3,
            x = 12
          },
          {
            y = 7,
            x = 12
          },
          {
            y = 9,
            x = 9
          },
          {
            y = 9,
            x = 3
          },
          {
            y = 11,
            x = 0
          },
          {
            y = 15,
            x = 0
          },
          {
            y = 17,
            x = 3
          },
          {
            y = 17,
            x = 9
          },
          {
            y = 15,
            x = 12
          }
        }
      },
      {
        points = {
          {
            y = 19,
            x = 6
          },
          {
            y = -1,
            x = 6
          }
        }
      }
    },
    width = 12
  },
  [37] = {
    character = "%",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 12
          }
        }
      },
      {
        points = {
          {
            y = 14,
            x = 6
          },
          {
            y = 10,
            x = 3
          },
          {
            y = 14,
            x = 0
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 14,
            x = 6
          }
        }
      },
      {
        points = {
          {
            y = 8,
            x = 9
          },
          {
            y = 4,
            x = 12
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 4,
            x = 6
          },
          {
            y = 8,
            x = 9
          }
        }
      }
    },
    width = 12
  },
  [38] = {
    character = "&",
    paths = {
      {
        points = {
          {
            y = 5,
            x = 12
          },
          {
            y = 0,
            x = 8
          },
          {
            y = 0,
            x = 2
          },
          {
            y = 4,
            x = 0
          },
          {
            y = 14,
            x = 9
          },
          {
            y = 18,
            x = 7
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 14,
            x = 1
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [39] = {
    character = "'",
    paths = {
      {
        points = {
          {
            y = 14,
            x = 1
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 18,
            x = 3
          }
        }
      }
    },
    width = 4
  },
  [40] = {
    character = "(",
    paths = {
      {
        points = {
          {
            y = -2,
            x = 6
          },
          {
            y = 4,
            x = 0
          },
          {
            y = 14,
            x = 0
          },
          {
            y = 20,
            x = 6
          }
        }
      }
    },
    width = 6
  },
  [41] = {
    character = ")",
    paths = {
      {
        points = {
          {
            y = -2,
            x = 0
          },
          {
            y = 4,
            x = 6
          },
          {
            y = 14,
            x = 6
          },
          {
            y = 20,
            x = 0
          }
        }
      }
    },
    width = 6
  },
  [42] = {
    character = "*",
    paths = {
      {
        points = {
          {
            y = 2,
            x = 3
          },
          {
            y = 16,
            x = 9
          }
        }
      },
      {
        points = {
          {
            y = 16,
            x = 3
          },
          {
            y = 2,
            x = 9
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 0
          },
          {
            y = 9,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [43] = {
    character = "+",
    paths = {
      {
        points = {
          {
            y = 2,
            x = 6
          },
          {
            y = 16,
            x = 6
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 0
          },
          {
            y = 9,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [44] = {
    character = ",",
    paths = {
      {
        points = {
          {
            y = -4,
            x = 4
          },
          {
            y = 1,
            x = 6
          },
          {
            y = 1,
            x = 6
          }
        }
      }
    },
    width = 16
  },
  [45] = {
    character = "-",
    paths = {
      {
        points = {
          {
            y = 9,
            x = 0
          },
          {
            y = 9,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [46] = {
    character = ".",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 6
          },
          {
            y = 0,
            x = 6
          },
          {
            y = 0,
            x = 6
          }
        }
      }
    },
    width = 16
  },
  [47] = {
    character = "/",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [48] = {
    character = "0",
    paths = {
      {
        points = {
          {
            y = 2,
            x = 1
          },
          {
            y = 16,
            x = 11
          }
        }
      },
      {
        points = {
          {
            y = 12,
            x = 12
          },
          {
            y = 6,
            x = 12
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 6,
            x = 0
          },
          {
            y = 12,
            x = 0
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 12,
            x = 12
          }
        }
      }
    },
    width = 16
  },
  [49] = {
    character = "1",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 3
          },
          {
            y = 0,
            x = 9
          }
        }
      },
      {
        points = {
          {
            y = 0,
            x = 6
          },
          {
            y = 18,
            x = 6
          },
          {
            y = 15,
            x = 3
          }
        }
      }
    },
    width = 16
  },
  [50] = {
    character = "2",
    paths = {
      {
        points = {
          {
            y = 15,
            x = 0
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 15,
            x = 12
          },
          {
            y = 11,
            x = 12
          },
          {
            y = 5,
            x = 2
          },
          {
            y = 0,
            x = 0
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 16
  },
  [51] = {
    character = "3",
    paths = {
      {
        points = {
          {
            y = 16,
            x = 0
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 15,
            x = 12
          },
          {
            y = 11,
            x = 12
          },
          {
            y = 9,
            x = 9
          },
          {
            y = 9,
            x = 3
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 9
          },
          {
            y = 7,
            x = 12
          },
          {
            y = 3,
            x = 12
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 2,
            x = 0
          }
        }
      }
    },
    width = 16
  },
  [52] = {
    character = "4",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 9
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 6,
            x = 0
          },
          {
            y = 6,
            x = 12
          }
        }
      }
    },
    width = 16
  },
  [53] = {
    character = "5",
    paths = {
      {
        points = {
          {
            y = 2,
            x = 0
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 2,
            x = 12
          },
          {
            y = 8,
            x = 12
          },
          {
            y = 10,
            x = 9
          },
          {
            y = 10,
            x = 3
          },
          {
            y = 9,
            x = 0
          },
          {
            y = 18,
            x = 2
          },
          {
            y = 18,
            x = 12
          }
        }
      }
    },
    width = 16
  },
  [54] = {
    character = "6",
    paths = {
      {
        points = {
          {
            y = 7,
            x = 0
          },
          {
            y = 10,
            x = 3
          },
          {
            y = 10,
            x = 9
          },
          {
            y = 7,
            x = 12
          },
          {
            y = 3,
            x = 12
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 3,
            x = 0
          },
          {
            y = 10,
            x = 0
          },
          {
            y = 15,
            x = 3
          },
          {
            y = 18,
            x = 7
          }
        }
      }
    },
    width = 16
  },
  [55] = {
    character = "7",
    paths = {
      {
        points = {
          {
            y = 18,
            x = 0
          },
          {
            y = 18,
            x = 12
          },
          {
            y = 0,
            x = 4
          }
        }
      }
    },
    width = 16
  },
  [56] = {
    character = "8",
    paths = {
      {
        points = {
          {
            y = 10,
            x = 3
          },
          {
            y = 13,
            x = 0
          },
          {
            y = 16,
            x = 0
          },
          {
            y = 19,
            x = 3
          },
          {
            y = 19,
            x = 9
          },
          {
            y = 16,
            x = 12
          },
          {
            y = 13,
            x = 12
          },
          {
            y = 10,
            x = 9
          },
          {
            y = 10,
            x = 3
          },
          {
            y = 7,
            x = 0
          },
          {
            y = 3,
            x = 0
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 3,
            x = 12
          },
          {
            y = 7,
            x = 12
          },
          {
            y = 10,
            x = 9
          }
        }
      }
    },
    width = 16
  },
  [57] = {
    character = "9",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 5
          },
          {
            y = 3,
            x = 9
          },
          {
            y = 8,
            x = 12
          },
          {
            y = 15,
            x = 12
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 15,
            x = 0
          },
          {
            y = 11,
            x = 0
          },
          {
            y = 8,
            x = 3
          },
          {
            y = 8,
            x = 9
          },
          {
            y = 11,
            x = 12
          }
        }
      }
    },
    width = 16
  },
  [58] = {
    character = ":",
    paths = {
      {
        points = {
          {
            y = 4,
            x = 1
          },
          {
            y = 4,
            x = 1
          }
        }
      },
      {
        points = {
          {
            y = 14,
            x = 1
          },
          {
            y = 14,
            x = 1
          }
        }
      }
    },
    width = 2
  },
  [59] = {
    character = ";",
    paths = {
      {
        points = {
          {
            y = -4,
            x = 1
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 0,
            x = 3
          }
        }
      },
      {
        points = {
          {
            y = 10,
            x = 3
          },
          {
            y = 10,
            x = 3
          }
        }
      }
    },
    width = 4
  },
  [60] = {
    character = "<",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 12
          },
          {
            y = 9,
            x = 0
          },
          {
            y = 18,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [61] = {
    character = "=",
    paths = {
      {
        points = {
          {
            y = 4,
            x = 0
          },
          {
            y = 4,
            x = 12
          }
        }
      },
      {
        points = {
          {
            y = 14,
            x = 0
          },
          {
            y = 14,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [62] = {
    character = ">",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 9,
            x = 12
          },
          {
            y = 18,
            x = 0
          }
        }
      }
    },
    width = 12
  },
  [63] = {
    character = "?",
    paths = {
      {
        points = {
          {
            y = 15,
            x = 0
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 15,
            x = 12
          },
          {
            y = 11,
            x = 12
          },
          {
            y = 7,
            x = 6
          },
          {
            y = 4,
            x = 6
          }
        }
      },
      {
        points = {
          {
            y = 0,
            x = 6
          },
          {
            y = 0,
            x = 6
          }
        }
      }
    },
    width = 12
  },
  [64] = {
    character = "@",
    paths = {
      {
        points = {
          {
            y = 2,
            x = 12
          },
          {
            y = 0,
            x = 10
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 3,
            x = 0
          },
          {
            y = 15,
            x = 0
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 15,
            x = 12
          },
          {
            y = 6,
            x = 12
          },
          {
            y = 6,
            x = 5
          },
          {
            y = 13,
            x = 5
          },
          {
            y = 13,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [65] = {
    character = "A",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 6
          },
          {
            y = 0,
            x = 12
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 3
          },
          {
            y = 9,
            x = 9
          }
        }
      }
    },
    width = 12
  },
  [66] = {
    character = "B",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 15,
            x = 12
          },
          {
            y = 12,
            x = 12
          },
          {
            y = 9,
            x = 9
          },
          {
            y = 9,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 9
          },
          {
            y = 6,
            x = 12
          },
          {
            y = 3,
            x = 12
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 0,
            x = 0
          }
        }
      }
    },
    width = 12
  },
  [67] = {
    character = "C",
    paths = {
      {
        points = {
          {
            y = 3,
            x = 12
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 3,
            x = 0
          },
          {
            y = 15,
            x = 0
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 15,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [68] = {
    character = "D",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 15,
            x = 12
          },
          {
            y = 3,
            x = 12
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 0,
            x = 0
          }
        }
      }
    },
    width = 12
  },
  [69] = {
    character = "E",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          },
          {
            y = 18,
            x = 12
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 0
          },
          {
            y = 9,
            x = 9
          }
        }
      },
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [70] = {
    character = "F",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          },
          {
            y = 18,
            x = 12
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 0
          },
          {
            y = 9,
            x = 9
          }
        }
      }
    },
    width = 12
  },
  [71] = {
    character = "G",
    paths = {
      {
        points = {
          {
            y = 15,
            x = 12
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 15,
            x = 0
          },
          {
            y = 3,
            x = 0
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 3,
            x = 12
          },
          {
            y = 8,
            x = 12
          },
          {
            y = 8,
            x = 5
          }
        }
      }
    },
    width = 12
  },
  [72] = {
    character = "H",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 0,
            x = 12
          },
          {
            y = 18,
            x = 12
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 0
          },
          {
            y = 9,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [73] = {
    character = "I",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 0,
            x = 8
          }
        }
      },
      {
        points = {
          {
            y = 0,
            x = 4
          },
          {
            y = 18,
            x = 4
          }
        }
      },
      {
        points = {
          {
            y = 18,
            x = 0
          },
          {
            y = 18,
            x = 8
          }
        }
      }
    },
    width = 8
  },
  [74] = {
    character = "J",
    paths = {
      {
        points = {
          {
            y = 2,
            x = 0
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 0,
            x = 5
          },
          {
            y = 2,
            x = 8
          },
          {
            y = 18,
            x = 8
          }
        }
      },
      {
        points = {
          {
            y = 18,
            x = 4
          },
          {
            y = 18,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [75] = {
    character = "K",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 18,
            x = 12
          },
          {
            y = 6,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 3
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [76] = {
    character = "L",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [77] = {
    character = "M",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          },
          {
            y = 5,
            x = 6
          },
          {
            y = 18,
            x = 12
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [78] = {
    character = "N",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          },
          {
            y = 0,
            x = 12
          },
          {
            y = 18,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [79] = {
    character = "O",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 3
          },
          {
            y = 3,
            x = 0
          },
          {
            y = 15,
            x = 0
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 15,
            x = 12
          },
          {
            y = 3,
            x = 12
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 0,
            x = 3
          }
        }
      }
    },
    width = 12
  },
  [80] = {
    character = "P",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 15,
            x = 12
          },
          {
            y = 11,
            x = 12
          },
          {
            y = 8,
            x = 9
          },
          {
            y = 8,
            x = 0
          }
        }
      }
    },
    width = 12
  },
  [81] = {
    character = "Q",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 3
          },
          {
            y = 3,
            x = 0
          },
          {
            y = 15,
            x = 0
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 15,
            x = 12
          },
          {
            y = 3,
            x = 12
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 0,
            x = 3
          }
        }
      },
      {
        points = {
          {
            y = 5,
            x = 7
          },
          {
            y = -2,
            x = 14
          }
        }
      }
    },
    width = 12
  },
  [82] = {
    character = "R",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 15,
            x = 12
          },
          {
            y = 11,
            x = 12
          },
          {
            y = 8,
            x = 9
          },
          {
            y = 8,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 8,
            x = 7
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [83] = {
    character = "S",
    paths = {
      {
        points = {
          {
            y = 2,
            x = 0
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 3,
            x = 12
          },
          {
            y = 6,
            x = 12
          },
          {
            y = 9,
            x = 9
          },
          {
            y = 9,
            x = 3
          },
          {
            y = 12,
            x = 0
          },
          {
            y = 15,
            x = 0
          },
          {
            y = 18,
            x = 3
          },
          {
            y = 18,
            x = 9
          },
          {
            y = 16,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [84] = {
    character = "T",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 6
          },
          {
            y = 18,
            x = 6
          }
        }
      },
      {
        points = {
          {
            y = 18,
            x = 0
          },
          {
            y = 18,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [85] = {
    character = "U",
    paths = {
      {
        points = {
          {
            y = 18,
            x = 0
          },
          {
            y = 3,
            x = 0
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 3,
            x = 12
          },
          {
            y = 18,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [86] = {
    character = "V",
    paths = {
      {
        points = {
          {
            y = 18,
            x = 0
          },
          {
            y = 0,
            x = 6
          },
          {
            y = 18,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [87] = {
    character = "W",
    paths = {
      {
        points = {
          {
            y = 18,
            x = 0
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 14,
            x = 6
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 18,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [88] = {
    character = "X",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 12
          }
        }
      },
      {
        points = {
          {
            y = 18,
            x = 0
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [89] = {
    character = "Y",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 6
          },
          {
            y = 7,
            x = 6
          },
          {
            y = 18,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 7,
            x = 6
          },
          {
            y = 18,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [90] = {
    character = "Z",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 12
          },
          {
            y = 18,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 0,
            x = 12
          },
          {
            y = 0,
            x = 0
          }
        }
      }
    },
    width = 12
  },
  [91] = {
    character = "[",
    paths = {
      {
        points = {
          {
            y = 20,
            x = 6
          },
          {
            y = 20,
            x = 0
          },
          {
            y = -2,
            x = 0
          },
          {
            y = -2,
            x = 6
          }
        }
      }
    },
    width = 6
  },
  [92] = {
    character = "\\",
    paths = {
      {
        points = {
          {
            y = 18,
            x = 0
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [93] = {
    character = [=[]]=],
    paths = {
      {
        points = {
          {
            y = -2,
            x = 0
          },
          {
            y = -2,
            x = 6
          },
          {
            y = 20,
            x = 6
          },
          {
            y = 20,
            x = 0
          }
        }
      }
    },
    width = 6
  },
  [94] = {
    character = "^",
    paths = {
      {
        points = {
          {
            y = 7,
            x = 0
          },
          {
            y = 16,
            x = 6
          },
          {
            y = 7,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [95] = {
    character = "_",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 0,
            x = 8
          }
        }
      }
    },
    width = 9
  },
  [97] = {
    character = "a",
    paths = {
      {
        points = {
          {
            y = 10,
            x = 0
          },
          {
            y = 12,
            x = 5
          },
          {
            y = 10,
            x = 11
          },
          {
            y = 2,
            x = 11
          },
          {
            y = 0,
            x = 8
          },
          {
            y = 0,
            x = 4
          },
          {
            y = 2,
            x = 0
          },
          {
            y = 5,
            x = 0
          },
          {
            y = 6,
            x = 11
          }
        }
      },
      {
        points = {
          {
            y = 2,
            x = 11
          },
          {
            y = 0,
            x = 13
          }
        }
      }
    },
    width = 13
  },
  [98] = {
    character = "b",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 0
          },
          {
            y = 11,
            x = 6
          },
          {
            y = 9,
            x = 12
          },
          {
            y = 2,
            x = 12
          },
          {
            y = 0,
            x = 6
          },
          {
            y = 2,
            x = 0
          }
        }
      }
    },
    width = 12
  },
  [99] = {
    character = "c",
    paths = {
      {
        points = {
          {
            y = 9,
            x = 11
          },
          {
            y = 11,
            x = 6
          },
          {
            y = 9,
            x = 0
          },
          {
            y = 2,
            x = 0
          },
          {
            y = 0,
            x = 6
          },
          {
            y = 2,
            x = 11
          }
        }
      }
    },
    width = 12
  },
  [100] = {
    character = "d",
    paths = {
      {
        points = {
          {
            y = 2,
            x = 12
          },
          {
            y = 0,
            x = 6
          },
          {
            y = 2,
            x = 0
          },
          {
            y = 9,
            x = 0
          },
          {
            y = 11,
            x = 6
          },
          {
            y = 9,
            x = 12
          }
        }
      },
      {
        points = {
          {
            y = 18,
            x = 12
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [101] = {
    character = "e",
    paths = {
      {
        points = {
          {
            y = 6,
            x = 0
          },
          {
            y = 7,
            x = 12
          },
          {
            y = 12,
            x = 9
          },
          {
            y = 12,
            x = 3
          },
          {
            y = 9,
            x = 0
          },
          {
            y = 2,
            x = 0
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 2,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [102] = {
    character = "f",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 4
          },
          {
            y = 16,
            x = 4
          },
          {
            y = 18,
            x = 8
          },
          {
            y = 16,
            x = 12
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 0
          },
          {
            y = 9,
            x = 8
          }
        }
      }
    },
    width = 12
  },
  [103] = {
    character = "g",
    paths = {
      {
        points = {
          {
            y = 2,
            x = 11
          },
          {
            y = 0,
            x = 6
          },
          {
            y = 2,
            x = 0
          },
          {
            y = 9,
            x = 0
          },
          {
            y = 11,
            x = 6
          },
          {
            y = 9,
            x = 11
          }
        }
      },
      {
        points = {
          {
            y = 11,
            x = 11
          },
          {
            y = -5,
            x = 11
          },
          {
            y = -7,
            x = 6
          },
          {
            y = -5,
            x = 0
          }
        }
      }
    },
    width = 12
  },
  [104] = {
    character = "h",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 0
          },
          {
            y = 11,
            x = 6
          },
          {
            y = 9,
            x = 12
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [105] = {
    character = "i",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 3
          },
          {
            y = 11,
            x = 3
          },
          {
            y = 11,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 18,
            x = 3
          },
          {
            y = 18,
            x = 3
          }
        }
      }
    },
    width = 3
  },
  [106] = {
    character = "j",
    paths = {
      {
        points = {
          {
            y = -5,
            x = 0
          },
          {
            y = -7,
            x = 4
          },
          {
            y = -5,
            x = 8
          },
          {
            y = 11,
            x = 8
          }
        }
      },
      {
        points = {
          {
            y = 18,
            x = 8
          },
          {
            y = 18,
            x = 8
          }
        }
      }
    },
    width = 8
  },
  [107] = {
    character = "k",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 18,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 5,
            x = 0
          },
          {
            y = 11,
            x = 12
          }
        }
      },
      {
        points = {
          {
            y = 7,
            x = 4
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [108] = {
    character = "l",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 1
          },
          {
            y = 0,
            x = 7
          }
        }
      },
      {
        points = {
          {
            y = 0,
            x = 4
          },
          {
            y = 18,
            x = 4
          },
          {
            y = 18,
            x = 1
          }
        }
      }
    },
    width = 8
  },
  [109] = {
    character = "m",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 12,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 0
          },
          {
            y = 12,
            x = 4
          },
          {
            y = 9,
            x = 6
          },
          {
            y = 0,
            x = 6
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 6
          },
          {
            y = 12,
            x = 10
          },
          {
            y = 9,
            x = 12
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [110] = {
    character = "n",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 11,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 8,
            x = 0
          },
          {
            y = 11,
            x = 6
          },
          {
            y = 8,
            x = 12
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [111] = {
    character = "o",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 6
          },
          {
            y = 2,
            x = 0
          },
          {
            y = 9,
            x = 0
          },
          {
            y = 11,
            x = 6
          },
          {
            y = 9,
            x = 12
          },
          {
            y = 2,
            x = 12
          },
          {
            y = 0,
            x = 6
          }
        }
      }
    },
    width = 12
  },
  [112] = {
    character = "p",
    paths = {
      {
        points = {
          {
            y = -7,
            x = 0
          },
          {
            y = 11,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 9,
            x = 0
          },
          {
            y = 11,
            x = 6
          },
          {
            y = 9,
            x = 12
          },
          {
            y = 2,
            x = 12
          },
          {
            y = 0,
            x = 6
          },
          {
            y = 2,
            x = 0
          }
        }
      }
    },
    width = 12
  },
  [113] = {
    character = "q",
    paths = {
      {
        points = {
          {
            y = 2,
            x = 11
          },
          {
            y = 0,
            x = 6
          },
          {
            y = 2,
            x = 0
          },
          {
            y = 9,
            x = 0
          },
          {
            y = 11,
            x = 6
          },
          {
            y = 9,
            x = 11
          }
        }
      },
      {
        points = {
          {
            y = 11,
            x = 11
          },
          {
            y = -6,
            x = 11
          },
          {
            y = -8,
            x = 13
          }
        }
      }
    },
    width = 12
  },
  [114] = {
    character = "r",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 11,
            x = 0
          }
        }
      },
      {
        points = {
          {
            y = 8,
            x = 0
          },
          {
            y = 11,
            x = 6
          },
          {
            y = 8,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [115] = {
    character = "s",
    paths = {
      {
        points = {
          {
            y = 2,
            x = 0
          },
          {
            y = 0,
            x = 6
          },
          {
            y = 2,
            x = 12
          },
          {
            y = 5,
            x = 12
          },
          {
            y = 7,
            x = 0
          },
          {
            y = 10,
            x = 0
          },
          {
            y = 12,
            x = 6
          },
          {
            y = 10,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [116] = {
    character = "t",
    paths = {
      {
        points = {
          {
            y = 2,
            x = 12
          },
          {
            y = 0,
            x = 8
          },
          {
            y = 2,
            x = 4
          },
          {
            y = 18,
            x = 4
          }
        }
      },
      {
        points = {
          {
            y = 11,
            x = 0
          },
          {
            y = 11,
            x = 8
          }
        }
      }
    },
    width = 12
  },
  [117] = {
    character = "u",
    paths = {
      {
        points = {
          {
            y = 11,
            x = 0
          },
          {
            y = 2,
            x = 0
          },
          {
            y = 0,
            x = 6
          },
          {
            y = 2,
            x = 12
          },
          {
            y = 11,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [118] = {
    character = "v",
    paths = {
      {
        points = {
          {
            y = 11,
            x = 0
          },
          {
            y = 0,
            x = 6
          },
          {
            y = 11,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [119] = {
    character = "w",
    paths = {
      {
        points = {
          {
            y = 11,
            x = 0
          },
          {
            y = 0,
            x = 3
          },
          {
            y = 8,
            x = 6
          },
          {
            y = 0,
            x = 9
          },
          {
            y = 11,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [120] = {
    character = "x",
    paths = {
      {
        points = {
          {
            y = 0,
            x = 0
          },
          {
            y = 11,
            x = 11
          }
        }
      },
      {
        points = {
          {
            y = 11,
            x = 0
          },
          {
            y = 0,
            x = 11
          }
        }
      }
    },
    width = 12
  },
  [121] = {
    character = "y",
    paths = {
      {
        points = {
          {
            y = 11,
            x = 0
          },
          {
            y = 1,
            x = 7
          }
        }
      },
      {
        points = {
          {
            y = -7,
            x = 3
          },
          {
            y = 11,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [122] = {
    character = "z",
    paths = {
      {
        points = {
          {
            y = 11,
            x = 0
          },
          {
            y = 11,
            x = 12
          },
          {
            y = 0,
            x = 0
          },
          {
            y = 0,
            x = 12
          }
        }
      }
    },
    width = 12
  },
  [123] = {
    character = "{",
    paths = {
      {
        points = {
          {
            y = -2,
            x = 8
          },
          {
            y = 1,
            x = 3
          },
          {
            y = 6,
            x = 3
          },
          {
            y = 9,
            x = 0
          },
          {
            y = 12,
            x = 3
          },
          {
            y = 17,
            x = 3
          },
          {
            y = 20,
            x = 8
          }
        }
      }
    },
    width = 8
  },
  [125] = {
    character = "}",
    paths = {
      {
        points = {
          {
            y = -2,
            x = 0
          },
          {
            y = 1,
            x = 5
          },
          {
            y = 6,
            x = 5
          },
          {
            y = 9,
            x = 8
          },
          {
            y = 12,
            x = 5
          },
          {
            y = 17,
            x = 5
          },
          {
            y = 20,
            x = 0
          }
        }
      }
    },
    width = 8
  }
}

return hp1345a_glyphs