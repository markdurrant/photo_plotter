require("pequod")

local hp1345a_glyphs = require("src.hp1345a_glyphs")

-- math.randomseed(os.time() * 99999999999999)

-- local paper = Paper({ width = 1000, height = 1000})
-- local blue_pen = Pen({ paper = paper, weight = 0.5, color = 'blue'})
-- local black_pen = Pen({ paper = paper, weight = 1.5, color = 'black'})

 
local render_type = function (str, top_left, pen, font_size, leading, tracking)
  local type_paths = {}
  
  local font_size = font_size or 12
  local leading = leading or font_size * 0.85
  local tracking = tracking or font_size / 3

  local active_location = top_left:move(0, font_size * -2):clone()

  -- set sizing ratio for font height and width
  -- 18 = raw glyph cap height
  local font_sizing = font_size / 18

  local render_glyph = function (glyph, location)
    for _, pa in ipairs(glyph.paths) do 
      local stroke = Path()

      for _, po in ipairs(pa.points) do
        stroke:add_points({ Point(po.x * font_sizing, po.y * font_sizing) })
      end

      stroke:move(location.x, location.y)

      if pen then
        stroke:draw_with(pen)
      end 

      table.insert(type_paths, stroke)
    end
  end 

  local render_line = function (line, active_location)
    active_location.x = top_left.x

    for char in line:gmatch(".") do
      -- get the right character from the glyphs table
      char = hp1345a_glyphs[char:byte(1)]

      -- replace unknown characters with spaces
      if char == nil then char = hp1345a_glyphs[32] end
      
      render_glyph(char, active_location)
      
      active_location.x = active_location.x + char.width * font_sizing + tracking
    end 

    active_location.y = active_location.y - font_size - leading
  end 

  for line in str:gmatch('([^\n]+)') do
    render_line(line, active_location)
  end

  return type_paths
end 

local fresh = [[Expedita consectetur unde. Perspiciatis ea molestiae
corporis libero impedit. Nisi adipisci omnis excepturi sint hic
atque reprehenderit eligendi animi.
 
Minus excepturi adipisci eos. Et nisi sed aut culpa iusto consequatur.
Eos illum quaerat neque ut possimus odio molestias recusandae.
 
Ad saepe nisi accusantium quis veritatis nobis velit explicabo quo.
Molestias aspernatur molestias hic aspernatur quo minima veritatis alias.
Quaerat omnis eum est quia eveniet repellat officiis.
Voluptate tempore debitis error sequi.
Placeat eos fugiat est officia fugiat ut ut facilis.]]

-- local rt = render_type(fresh, Point(10, paper.height - 10), black_pen, 18)

-- for _, p in ipairs(rt) do
--   p:draw_with(black_pen)
-- end

-- paper:save_svg("draw_line.svg")

return render_type