require("pequod")
local render_type = require("src.type")

local render = function (paths, opts)
  local paper = Paper({ width = 210, height = 297 })
    
  local black_pen = Pen({ paper = paper, weight = 0.5, color = 'black', position = 1 })
  local blue_pen = Pen({ paper = paper, weight = 0.5, color = '#09f', position = 2 })
  local green_pen = Pen({ paper = paper, weight = 0.5, color = '#0fc', position = 3 })
  local guide_pen = Pen({ paper = paper, weight = 1, color = '#f90', position = 4 })

  if paths.vertical_lines then 
    for _, p in ipairs(paths.vertical_lines) do
      p:draw_with(blue_pen)
    end 
  end

  if paths.scratch then
    for _, p in ipairs(paths.scratch) do
      p:draw_with(green_pen)
    end 
  end

  if paths.grid_paths then
    for _, p in ipairs(paths.grid_paths) do
      if p.points[1].y > opts.output_width / 2 + math.random(opts.output_width / -2, opts.output_width / 2) then
        p:draw_with(green_pen)
      else
        p:draw_with(blue_pen)
      end
    end 

    for i = #paper.paths, 1, -1 do
      local p = paper.paths[i]
      local prev = paper.paths[i - 1] or {}

      if prev.pen == p.pen and p.points[1] == prev.points[2] then 
        p.points[1].x = prev.points[1].x

        table.remove(paper.paths, i - 1)
      end
    end
  end

  -- moves all paths with a specific pen to the end of the paths table
  local sort_pen = function (paths)
    table.sort(
      paths,
      function (a, b) return a.pen.position > b.pen.position end 
    )
  end 
  sort_pen(paper.paths)

  if paths.edges then
    for _, p in ipairs(paths.edges) do
      p:draw_with(black_pen)
    end 
  end 

  local outline = Path({
    Point(0, 0),
    Point(opts.output_width, 0),
    Point(opts.output_width, opts.output_width),
    Point(0, opts.output_width),
    Point(0, 0),
    Point(opts.output_width, 0),
    Point(opts.output_width, opts.output_width),
    Point(0, opts.output_width),
    Point(0, 0)
  }):draw_with(black_pen)

  -- move the square
  for _, p in ipairs(paper.paths) do
    p:move(15, 58)
  end

  -- draw a6 guide for dev
  local a6 = { width = 105, height = 148 }

  local guide_path = Path({
    Point(0, 0),
    Point(a6.width, 0),
    Point(a6.width, a6.height),
    Point(0, a6.height)
  }):close():draw_with(black_pen)

  render_type([[UNWEDDING 2019]], Point(28, 48), black_pen, 3.5)
  render_type([[SUPERDUPER.COOL]], Point(34, 38), black_pen, 2.5)

  -- move EVERYTHING
  for _, p in ipairs(paper.paths) do
    p:move(80, 80)
  end

  paper:save_svg('output/output.svg')
  paper:save_hpgl('output/output.hpgl')
end 

return render