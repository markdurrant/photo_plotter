-- Trace an image and return a table of Pequod paths  
----------------------------------------------------
require("LuaXML")
require("pequod")




local image_to_svg = function(img, opts)
  local svg = ""
  local tmp_input = 'tmp_files/tmp.pbm'
  local tmp_output = 'tmp_files/tmp.svg'

  -- autotrace comand 
  local autotrace = ("autotrace --input-format pbm --output-format plot-svg --output-file %s --centerline --line-threshold %s --error-threshold %s --despeckle-level 10 --despeckle-tightness 0 %s"):format(tmp_output, tostring(opts.auto_line), tostring(opts.auto_error), tmp_input)

  -- Write tmp raster image to file 
  img:write_to_file(tmp_input)

  -- Run Potrace and save temp SVG
  os.execute(autotrace)

  -- Read SVG file, and return  
  tmp_output = io.open(tmp_output, "rb")
  svg = tmp_output:read("*a")
  
  tmp_output:close()

  return svg
end 

local parse_svg = function (svg, opts)
  local paths = {}

  -- parse points string from polygon or polyline 
  local parse_points = function (points_str)
    local points_table = {}

    for p in points_str:gmatch("[^%s]+") do 
      local x, y = p:match("(.*),(.*)")

      table.insert(
        points_table,
        Point(
          tonumber(x) / opts.process_width * opts.output_width,
          tonumber(y) / opts.process_width * opts.output_width
        )
      )
    end

    return points_table
  end

  -- use luaxml to turn svg into a table
  -- find the top level <g> tag
  svg = xml.eval(svg)
  svg = svg:find("g")

  -- itterate over the <g> table 
  for _, tag in ipairs(svg) do
    local p = Path()

    if tag[0] == "line" then 
      p:add_points({
        Point(
          tonumber(tag.x1) / opts.process_width * opts.output_width, 
          tonumber(tag.y1) / opts.process_width * opts.output_width
        ),
        Point(
          tonumber(tag.x2) / opts.process_width * opts.output_width,
          tonumber(tag.y2) / opts.process_width * opts.output_width
        ) 
      })
    elseif tag[0] == "polyline" or tag[0] == "polygon" then 
      p:add_points(parse_points(tag.points))
      
      if tag[0] == "polygon" then 
        p:close()
      end
    end
    
    if p.length > opts.min_path_length then 
      table.insert(paths, p)
    end
  end

  return paths
end

local image_to_paths = function (img, opts)
  local svg = image_to_svg(img, opts)
  local paths = parse_svg(svg, opts)

  return paths 
end

return image_to_paths