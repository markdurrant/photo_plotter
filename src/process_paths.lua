-- Process paths, FOR ART
-------------------------
require("pequod")

local process_paths = {}

process_paths.vertical_lines = function (paths, opts)
  local vertical_lines = {}
  local v_lines = {}
  local num_lines = 100

  for l = 0, num_lines do
    local p = Path({
      Point(-1, opts.output_width / num_lines * l),
      Point(opts.output_width + 1, opts.output_width / num_lines * l)
    })

    table.insert(v_lines, p)
  end 

  for _, v in ipairs(v_lines) do
    for _, p in ipairs(paths) do
      local ints = v:intersections(p)

      if ints then
        -- sort left
        table.sort(ints, function (a, b) return a.x < b.x end )

        if ints[1] == ints[2] then
          table.remove(ints, 1)
        end

        for i, p in ipairs(ints) do
          if i > 1 and i % 2 == 0 then
            local p = Path({ ints[i - 1], p })

            table.insert(vertical_lines, p)
          end
        end
      end
    end 
  end

  return vertical_lines
end

process_paths.scratch = function (paths, opts)
  local scratch_this = function (path)
    local scratch = Path()

    local np = #path.points
    local np_max = 200

    if np > np_max then np = np_max end 

    for i = 1, np do
      local r = math.random(1, #path.points)
      local p = path.points[r]

      scratch:add_points({ p })
      table.remove(path.points, r)
    end

    return scratch
  end
  
  local scratch_lines = {}

  for _, p in ipairs(paths) do
    table.insert(scratch_lines, scratch_this(p))
  end 

  return scratch_lines
end

process_paths.grid_points = function (grid, opts)
  local grid_points = {}
  local scale_factor = opts.output_width / opts.grid_size

  for y, r in ipairs(grid) do
    for x, v in ipairs(r) do
        x = x
        y = y

        local p = Point(
          x * scale_factor - scale_factor / 2,
          opts.output_width - y * scale_factor + scale_factor / 2
        )

        p.v = v

        table.insert(grid_points, p)
    end
  end

  return grid_points
end 

process_paths.grid_short_lines = function (grid, opts)
  local line_paths = {}
  local grid_points = process_paths.grid_points(grid, opts)
  local line_length = opts.output_width / opts.grid_size

  for r = 1, opts.grid_size do
    for c = 1, opts.grid_size do
      local p = grid_points[(r - 1) * opts.grid_size + c]

      if p.v > 128 then
        table.insert(
          line_paths,
          Path({
            p:clone():move(-line_length / 2, 0),
            p:clone():move(line_length / 2, 0)
          })
        )
      end      
    end
  end

  return line_paths
end 

return process_paths