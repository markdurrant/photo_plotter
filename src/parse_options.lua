-- Parse command line options
-----------------------------

local argparse = require('argparse')

local parser = argparse('Photo Plotter', 'These are the things!')

      parser:option('-I --input_file', 'Input image file')

      parser:option('-W --process_width', 'Processing width', 300, tonumber)
      parser:option('-O --output_width', 'Processing width', 75, tonumber)
      parser:option('-S --edge_sigma', 'Edge detection sigma', 1.2, tonumber)
      parser:option('-P --edge_precision', 'Edge detection precision', 1, tonumber)
      parser:option('-F --edge_factor', 'Edge detection factor', 128, tonumber)
      parser:option('-T --edge_threshold', 'Edge threshold', 128, tonumber)
      parser:option('-C --edge_contrast', 'Edge contrast', 1.5, tonumber)
      parser:option("-L --auto_line", "Autotrace line threshold", 360, tonumber)
      parser:option("-E --auto_error", "Autotrace error threshold", 0.05, tonumber)
      parser:option("-M --min_path_length", "Min path length", 1.5, tonumber)
      parser:option('-t --fill_threshold', 'Fill threshold', 103, tonumber)
      parser:option('-G --grid_size', 'Size of grid', 50, tonumber)

local opts = parser:parse()

-- if no image file specified 
if not opts.input_file then
  os.execute("gphoto2 --capture-image-and-download --keep --stdout > captured_images/capture.jpg")
  print("Image captured")

  opts.input_file = 'captured_images/capture.jpg'
end 

return opts