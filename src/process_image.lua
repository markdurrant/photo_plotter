-- All image loading & processing
---------------------------------

local vips = require("vips")

local process_image = {}

process_image.load_image = function (opts)
  -- grab the image and convert to vips object
  local img = vips.Image.new_from_file(opts.input_file)  

  -- rotate the image to be horizontal
  img = img:autorot()

  -- crop to square
  if img:width() < img:height() then 
    img = img:smartcrop(img:width(), img:width())
  else
    img = img:smartcrop(img:height(), img:height())
  end

  -- resize to a more manageble size 
  img = img:resize(opts.process_width / img:width())

  return img
end

process_image.detect_edges = function (img, opts)
  -- adujst contrast
  img = img * opts.edge_contrast - (255 * opts.edge_contrast - 255)

  -- canny edge detection
  img = img:canny({ sigma = opts.edge_sigma, precision = opts.edge_precision })
  img = img * opts.edge_factor

  -- make monochrome
  img = img:colourspace('b-w')
  
  -- invert
  img = img:invert()
  
  -- make reduce B & W
  img = img:relational_const("more", opts.edge_threshold)

  return img
end

process_image.threshold = function (img, opts)
  -- make monochrome
  img = img:colourspace('b-w')

  -- reduce to B & W
  img = img:relational_const("more", opts.fill_threshold)

  return img
end

process_image.get_grid = function (img, opts)
  local grid = {}
  local tmp_output = "tmp_files/tmp.txt"

  -- make monochrome
  img = img:colourspace('b-w')

  -- invert
  img = img:invert()

  -- resize
  img = img:resize(opts.grid_size / img:width())

  -- write img matrix to file
  img:matrixsave(tmp_output)

  -- read img matrix and delete file
  local output_file = io.open(tmp_output, "rb")
  local output_content = output_file:read("*a")
  output_file:close()
  os.remove(tmp_output)

  -- setup rows & cols
  local r, c = 1, 1

  -- parse matrix output and save to table
  for line in output_content:gmatch("([^\n]*)\n?") do
    if #line > 10 then
      grid[r] = {}
      c = 1

      for v in line:gmatch("%S+") do
        grid[r][c] = tonumber(v)

        c = c + 1
      end

      r = r + 1
    end
  end

  return grid
end

return process_image